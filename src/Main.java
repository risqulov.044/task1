import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        task1();
    }

    public static void task1() throws InterruptedException {
        AtomicInteger counter1 = new AtomicInteger(0);
        AtomicInteger counter2 = new AtomicInteger(0);

        ExecutorService pool = Executors.newFixedThreadPool(10);

        Runnable action = () -> {
            counter1.incrementAndGet();
            counter2.incrementAndGet();
        };

        for (int i = 0; i < 100_000; i++) {
            pool.execute(action);
        }

        pool.awaitTermination(2, TimeUnit.SECONDS);

        System.out.println(counter1.get());
        System.out.println(counter2.get());

        pool.shutdown();
    }

}